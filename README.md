## Spring Boot Where is My Driver Appplication



## Requirements

1. Java - 1.8.x reason is I'm fluent on Java programming language :D
2. Maven - 3.x.x reason is maven is one of familiar building tools in java
3. MongoDB - v3.6.4 reason is mongodb support geospatial
4. Spring Boot - 2.0.x reason is Spring one of best framework in Java
5. Swagger - Just for documentation of APIs
6. Rest Assured - reason is one of familiar API testing tools in Java

## Steps to Setup without docker

**1. Clone the application**

```bash
git clone https://gitlab.com/teguheka/where-is-my-driver.git
```

**2. Running database**

install mongodb first and running mongodb with command like below

```bash
mongod
```

**3. Build and run the app using maven**

```bash
cd where-is-my-driver
mvn package -DskipTests
java -jar target/whereismydriver-0.0.1-SNAPSHOT.jar
```

**4. Running application**

Alternatively, you can run the app directly without packaging it like so -

```bash
mvn spring-boot:run
```

**5. How to testing**

open this url 
```
http://localhost:8080/swagger-ui.html
```

***A. Find drivers by distance***
- latitude is required
- longitude is required
- radius is optional (default 500m) measure in meters
- limit is optional (default 10 data)

cUrl for find drivers

```
curl -X GET \
  'http://localhost:8080/drivers?latitude=-6.601934&longitude=106.805637&radius=10000&limit=10' \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 99b5a120-4bff-4b36-83d8-2eea4ada879e'
```
***B. Update driver location***

cUrl for Update driver location
```
curl -X PUT \
  http://localhost:8080/drivers/51/location \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 84060c4c-32d8-4b9a-b953-d2d69b92ff38' \
  -d '{
	"latitude" : "-6.601934",
	"longitude" : "106.805637",
	"accuracy" : "0.7"
}'
```

note: i've generated some data in Bogor area when application started


## Steps to Running Tests

type below command in your terminal
```
mvn integration-test -P integration
```

And you can got a result like below
![test](img/result_test1.png)


## Steps to Setup with docker

```
docker pull mongo
docker run -p 27017:27017
docker inspect mongo | grep IPAddress [type in terminal docker mongo]
```

![test](img/grep_ip_mongodb.png)

and replace value spring.data.mongodb.host with IPAdress value,
then continue type command in terminal

```
docker run --name="where-is-my-driver_web" --publish 9001:8080 where-is-my-driver_web
```

then open in browser
```
http://localhost:9001/swagger-ui.html
```