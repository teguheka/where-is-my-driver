package com.teguh.gojek.whereismydriver.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MathUtilTests {
    @Test
    public void roundValue() {
        double value1 = 201d;
        double value2 = 200.98d;
        assertTrue(value1 == MathUtil.round(value2, 0));
    }
}
