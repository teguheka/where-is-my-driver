package com.teguh.gojek.whereismydriver.service;

import com.teguh.gojek.whereismydriver.entity.Driver;
import com.teguh.gojek.whereismydriver.entity.DriverUpdateLocationRequest;
import com.teguh.gojek.whereismydriver.repository.DriverRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DriverServiceTests {

    @Autowired
    private DriverService driverService;
    @Autowired
    private DriverRepository driverRepository;

    @Test
    public void updateDriverLoc() {
        List<Driver> drivers = driverRepository.findAll();
        Driver driver = drivers.get(0);

        DriverUpdateLocationRequest request = new DriverUpdateLocationRequest();
        request.setLatitude(-6.598034);
        request.setLongitude(106.797371);
        request.setAccuracy(0.7d);
        driverService.updateDriverLoc(driver.getId(), request);

        Driver updatedDriver = driverService.get(driver.getId());

        assertEquals(new GeoJsonPoint(request.getLongitude(), request.getLatitude()).getCoordinates(), updatedDriver.getLocation().getCoordinates());
        assertEquals(request.getAccuracy(), updatedDriver.getAccuracy());
    }
}
