package com.teguh.gojek.whereismydriver;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.teguh.gojek.whereismydriver.entity.Driver;
import com.teguh.gojek.whereismydriver.repository.DriverRepository;
import net.minidev.json.JSONValue;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WhereismydriverApplicationTests {

	@Autowired private DriverRepository driverRepository;

	@LocalServerPort
	int port;

	private static final String DRIVERS_RESOURCE = "/drivers";
	private static final String DRIVER_RESOURCE = "/drivers/{id}";
	private static final String DRIVER_LOC_RESOURCE = "/drivers/{id}/location";
	private static final Long NON_EXISTING_ID = 999999999L;

	private List<Driver> drivers = new ArrayList<>();

	@Before
	public void setUp() {
		drivers = driverRepository.findAll();
		RestAssured.port = port;
	}

	@Test
	public void testGetDriverById() {
		Long driverId = drivers.get(0).getId();
		given()
				.port(port)
				.contentType(ContentType.JSON)
				.when()
				.get(DRIVER_RESOURCE, driverId)
				.then()
				.statusCode(HttpStatus.SC_OK);
	}

	@Test
	public void testFindDriverByDistance() {
		given()
				.port(port)
				.queryParam("latitude", "-6.601934")
				.queryParam("longitude", "106.805637")
				.queryParam("radius", "500")
				.queryParam("limit", "10")
				.when()
				.get(DRIVERS_RESOURCE)
				.then()
				.statusCode(HttpStatus.SC_OK);
	}

	@Test
	public void testUpdateDriverLocation() throws IOException {
		Driver driver = drivers.get(0);

		//near Kebun Raya Bogor
		String bodyRequest = prepareUpdateLocBody("-6.602456", "106.801506", "0.1");

		given()
				.port(port)
				.body(bodyRequest)
				.contentType(ContentType.JSON)
				.when()
				.put(DRIVER_LOC_RESOURCE, driver.getId())
				.then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
	}

	@Test
	public void getDriverByIdShouldBeNotFoundIfNonExistingDriverId() {
		given()
				.port(port)
				.contentType(ContentType.JSON)
				.when()
				.get(DRIVER_RESOURCE, NON_EXISTING_ID)
				.then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void updateDriverLocShouldBeNotFoundIfNonExistingDriverId() throws IOException {
		String bodyRequest = prepareUpdateLocBody("-6.601638", "106.805105", "0.7");

		given()
				.port(port)
				.body(bodyRequest)
				.contentType(ContentType.JSON)
				.when()
				.put(DRIVER_LOC_RESOURCE, NON_EXISTING_ID)
				.then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}


	private String prepareUpdateLocBody(String latitude, String longitude, String accuracy) throws IOException {
		Map map = new LinkedHashMap();
		map.put("latitude", latitude);
		map.put("longitude", longitude);
		map.put("accuracy", accuracy);
		StringWriter out = new StringWriter();
		JSONValue.writeJSONString(map, out);
		return out.toString();
	}
}
