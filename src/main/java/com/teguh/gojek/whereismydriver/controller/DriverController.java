package com.teguh.gojek.whereismydriver.controller;

import com.teguh.gojek.whereismydriver.entity.Driver;
import com.teguh.gojek.whereismydriver.entity.DriverUpdateLocationRequest;
import com.teguh.gojek.whereismydriver.service.DriverService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "drivers", description = "Operations pertaining with drivers")
@RestController
public class DriverController {
    private static final Logger logger = LoggerFactory.getLogger(DriverController.class);

    @Autowired
    private DriverService driverService;

    /**
     * Get data driver by id
     *
     * @param Long id
     * @return
     */
    @ApiOperation(value = "Get specified driver with id")
    @GetMapping("/drivers/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Driver findById(@PathVariable Long id) {
        logger.info("get detail driver with id {}", id);
        return driverService.get(id);
    }

    /**
     * Update driver location
     *
     * @param Long    id
     * @param request
     */
    @ApiOperation(value = "Update driver location")
    @PutMapping("/drivers/{id}/location")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateLoc(@PathVariable Long id, @RequestBody DriverUpdateLocationRequest request) {
        logger.info("update driver location");
        driverService.updateDriverLoc(id, request);
    }

    /**
     * @param latitude  required
     * @param longitude required
     * @param radius    optional, default is 500 meters
     * @param limit     optional, default is 10 data
     * @return list of drivers
     */
    @ApiOperation(value = "View a list of drivers")
    @GetMapping("/drivers")
    @ResponseStatus(HttpStatus.OK)
    public List<Driver> findByDistance(@RequestParam Double longitude, @RequestParam Double latitude,
                                       @RequestParam(required = false, defaultValue = "500") Integer radius,
                                       @RequestParam(required = false, defaultValue = "10") Integer limit) {
        return driverService.findByDistance(longitude, latitude, radius, limit);
    }
}
