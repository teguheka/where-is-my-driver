package com.teguh.gojek.whereismydriver.repository;

import com.teguh.gojek.whereismydriver.entity.Driver;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DriverRepository extends MongoRepository<Driver, Long> {
    GeoResults<Driver> findByLocationNear(Point location, Distance distance);
}
