package com.teguh.gojek.whereismydriver.service;

import com.teguh.gojek.whereismydriver.entity.Driver;
import com.teguh.gojek.whereismydriver.entity.DriverUpdateLocationRequest;
import com.teguh.gojek.whereismydriver.exception.DataNotFoundException;
import com.teguh.gojek.whereismydriver.repository.DriverRepository;
import com.teguh.gojek.whereismydriver.util.MathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DriverService {
    @Autowired
    private DriverRepository driverRepository;

    public void updateDriverLoc(Long id, DriverUpdateLocationRequest request) {
        Driver driver = get(id);

        GeoJsonPoint point = new GeoJsonPoint(request.getLongitude(), request.getLatitude());
        driver.setLocation(point);
        driver.setAccuracy(request.getAccuracy());
        driverRepository.save(driver);
    }

    public Driver get(Long id) {
        Driver driver = driverRepository.findById(id).orElse(null);
        if (driver == null) {
            throw new DataNotFoundException("No driver with specified id");
        }
        return driver;
    }

    public List<Driver> findByDistance(Double longitude, Double latitude, Integer radius, Integer limit) {
        Point basePoint = new Point(longitude, latitude);
        Distance inRadius = new Distance(radius.doubleValue() / 1000, Metrics.KILOMETERS);
        GeoResults<Driver> driverGeoResults = driverRepository.findByLocationNear(basePoint, inRadius);

        List<Driver> drivers = new ArrayList<>();
        int totalData = driverGeoResults.getContent().size() > limit ? limit : driverGeoResults.getContent().size();
        for (int i = 0; i < totalData; i++) {
            Driver driver = driverGeoResults.getContent().get(i).getContent();
            driver.setDistance(MathUtil.round(driverGeoResults.getContent().get(i).getDistance().getValue() * 1000, 0));
            drivers.add(driver);
        }
        return drivers;
    }
}
