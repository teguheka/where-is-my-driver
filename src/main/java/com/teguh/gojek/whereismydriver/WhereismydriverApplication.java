package com.teguh.gojek.whereismydriver;

import com.teguh.gojek.whereismydriver.entity.Driver;
import com.teguh.gojek.whereismydriver.repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class WhereismydriverApplication implements CommandLineRunner {

    @Autowired
    private DriverRepository driverRepository;

    public static void main(String[] args) {
        SpringApplication.run(WhereismydriverApplication.class, args);
    }

    private Map<String, GeoJsonPoint> mockLocations() {
        Map<String, GeoJsonPoint> locations = new HashMap<>();
        locations.put("Istana Bogor", new GeoJsonPoint(106.797370, -6.598033));
        locations.put("Hotel Salak", new GeoJsonPoint(106.793904, -6.594412));
        locations.put("Tugu Kujang", new GeoJsonPoint(106.805105, -6.601638));
        locations.put("Lapangan Sempur", new GeoJsonPoint(106.800682, -6.591662));
        locations.put("Stasiun Bogor", new GeoJsonPoint(106.790361, -6.596030));
        locations.put("Kantor Lurah Cikaret", new GeoJsonPoint(106.785190, -6.617686));
        locations.put("Gumati Water Park", new GeoJsonPoint(106.843215, -6.586461));
        locations.put("Jungle Land", new GeoJsonPoint(106.894236, -6.574108));
        locations.put("Cibinong", new GeoJsonPoint(106.833382, -6.496872));
        locations.put("Jakarta", new GeoJsonPoint(106.836643, -6.218240));
        return locations;
    }

    @Override
    public void run(String... args) throws Exception {
        driverRepository.deleteAll();

        Long id = 1L;
        for (Map.Entry<String, GeoJsonPoint> driver : mockLocations().entrySet()) {
            GeoJsonPoint point = driver.getValue();
            Driver d = new Driver(id, driver.getKey(), point);
            driverRepository.save(d);
            id++;
        }

        System.out.println("Driver found with findAll():");
        System.out.println("-------------------------------");
        for (Driver driver : driverRepository.findAll()) {
            System.out.println(driver);
        }

        System.out.println();
        System.out.println("Driver found within 10K of longitude = '106.805637' ,latitude = '-6.601934'");
        System.out.println("--------------------------------");
        for (GeoResult<Driver> driver : driverRepository.findByLocationNear(new Point(106.805637, -6.601934), new Distance(10, Metrics.KILOMETERS))) {
            System.out.println(driver.getContent());
        }
        System.out.println();
    }
}
